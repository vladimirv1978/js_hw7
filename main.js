let mass = ["hello", "world", 23, "23", null, true];
const allTypes = ["string", "number", "boolean", "object"];

// С помощью "filter"-------------------
function filterBy(arrDate, type) {
  let newArr = arrDate.filter((element) => typeof(element) !== type);
  return console.log(`New array, no type data "${type}":   `, newArr);
}

allTypes.forEach(type => filterBy(mass, type));
//---------------------------------------

//С помощью "forEach"--------------------------
// let newArr = [];

// function filterBy(arrDate, type) {
//   if (typeof arrDate !== type) {
//     newArr.push(arrDate);
//   }
// }

// allTypes.forEach((type) => {
//   mass.forEach((element) => filterBy(element, type));
//   console.log(`New array, no type data "${type}":   `, newArr);
//   newArr = [];
// });
//-----------------------------------------

// С помощью "for of"-------------------
// function filterBy(arrDate, type) {
//   let newArr = [];
//   for (let arrElement of arrDate) {
//     if (typeof(arrElement) !== type) {
//       newArr.push(arrElement);
//     };
//   };
//   return console.log(`New array, no type data "${type}":   `, newArr);
// };

// allTypes.forEach(type => filterBy(mass, type));
//---------------------------------------

// С помощью "forEach" в теле функции------------
// function filterBy(arrDate, type) {
//   let newArr = [];
//   arrDate.forEach((element) => {
//     if (typeof(element) !== type) {
//       newArr.push(element);
//     }
//   });
//   return console.log(`New array, no type data "${type}":   `, newArr);
// }

// allTypes.forEach(type => filterBy(mass, type));
//---------------------------------------

